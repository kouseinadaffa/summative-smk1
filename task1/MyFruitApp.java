import java.util.Scanner;

public class MyFruitApp {
    public static void main(String[] args){
        Scanner input1 = new Scanner(System.in);
        System.out.print("Masukkan nama buah: ");
        String namaBuah = input1.nextLine();
        String namaBuahLowerCase = namaBuah.toLowerCase();

        Pisang p = new Pisang();
        Mangga m = new Mangga();
        Durian d = new Durian();

        if (namaBuahLowerCase.equals("pisang")) {
            p.tampilMakanan();
        } else if (namaBuahLowerCase.equals("mangga")) {
            m.tampilMakanan();
        } else if (namaBuahLowerCase.equals("durian")) {
            d.tampilMakanan();
        } else {
            System.out.println("Buah tidak dikenal");
        }
    }
}